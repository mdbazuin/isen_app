package com.mdbazuin.autocoffeemachine;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private double boilerTemperature;
    private double lightSensor;
    private double waterHeight;

    private String machineStatus;
    private boolean machineIsIdle;
    private int brewingTarget;
    private int brewingCurrentCup;
    private boolean brewingIsStrong;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button makeCoffeeButton = (Button) findViewById(R.id.makeCoffeeButton);
        makeCoffeeButton.setOnClickListener(this);

        this.requestQueue = Volley.newRequestQueue(this);
        this.requestQueue.start();

        this.boilerTemperature = 0.0;
        this.lightSensor = 0.0;
        this.waterHeight = 0.0;

        this.machineStatus = "IDLE";
        this.machineIsIdle = true;
        this.brewingTarget = 0;
        this.brewingCurrentCup = 0;
        this.brewingIsStrong = false;

        this.updateView();

        final MainActivity self = this;

        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        self.pollStatusRequest();
                    }
                },
                0,
                3000 // 3s
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.makeCoffeeButton: {
                final Button makeCoffeeButton = (Button) findViewById(R.id.makeCoffeeButton);
                makeCoffeeButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                makeCoffeeButton.setBackground(getDrawable(R.drawable.roundedpendingrequest));
                EditText coffeeIPView = (EditText) findViewById(R.id.coffeemachineIPText);
                EditText totalCupsView = (EditText) findViewById(R.id.totalCupsNumberEditText);
                ToggleButton coffeeStrengthButton = (ToggleButton) findViewById(R.id.coffeeStrenghToggleButton);
                final MainActivity self = this;
                try {
                    JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                            "http://" + coffeeIPView.getText() +":5000" + "/api/v1/coffee/brew",
                            new JSONObject("{\"cups\": " + totalCupsView.getText() + ", \"is_strong\": " + coffeeStrengthButton.isChecked() +"}"),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        if (!response.getBoolean("started_brewing")) {
                                            makeCoffeeButton.setBackground(getDrawable(R.drawable.roundedfailedrequest));
                                            makeCoffeeButton.setTextColor(getResources().getColor(R.color.colorAccent));
                                            makeCoffeeButton.setText(response.getString("msg"));
                                        } else {
                                            self.pollStatusRequest();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError err) {
                                    makeCoffeeButton.setBackground(getDrawable(R.drawable.roundedfailedrequest));
                                    makeCoffeeButton.setTextColor(getResources().getColor(R.color.colorAccent));
                                    err.printStackTrace();
                                }
                            });
                    req.setRetryPolicy(new DefaultRetryPolicy(600*1000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    this.requestQueue.add(req);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                break;
            }
        }
    }

    public void pollStatusRequest() {
        RequestQueue queue = Volley.newRequestQueue(this);
        EditText coffeeIPView = (EditText) findViewById(R.id.coffeemachineIPText);
        final MainActivity self = this;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                "http://"+ coffeeIPView.getText()+":5000/api/v1/machine/status",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            self.boilerTemperature = response.getJSONObject("sensors").getDouble("boiler_temperature");
                            self.waterHeight = response.getJSONObject("sensors").getDouble("water_height");
                            self.lightSensor = response.getJSONObject("sensors").getDouble("light_sensor");
                            self.machineStatus = response.getString("status");
                            self.machineIsIdle = response.getBoolean("is_idle");
                            self.brewingTarget = response.getJSONObject("brewing").getInt("target");
                            self.brewingCurrentCup = response.getJSONObject("brewing").getInt("current_cup");
                            self.brewingIsStrong = response.getJSONObject("brewing").getBoolean("is_strong");
                            self.updateView();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError err) {
                        err.printStackTrace();
                        return;
                    }
                });
        this.requestQueue.add(req);
    }

    public void updateView() {
        TextView temperature = (TextView) findViewById(R.id.tempVariableTextView);
        TextView waterHeight = (TextView) findViewById(R.id.waterHeightVariableTextView);
        TextView lightSensor = (TextView) findViewById(R.id.lightSensorVariableTextView);

        temperature.setText(Double.toString(this.boilerTemperature));
        waterHeight.setText(Double.toString(this.waterHeight));
        lightSensor.setText(Double.toString(this.lightSensor));

        Button makeCoffeeButton = (Button) findViewById(R.id.makeCoffeeButton);
        System.out.println(this.machineStatus);
        if(!this.machineStatus.equals("IDLE")) {
            makeCoffeeButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            makeCoffeeButton.setBackground(getDrawable(R.drawable.roundedpendingrequest));
            String brewingText = ""+ Integer.toString(this.brewingCurrentCup)+"/"+ Integer.toString(this.brewingTarget);
            if (this.brewingIsStrong) {
                brewingText += " Sterk";
            }
            makeCoffeeButton.setText(brewingText);
        } else {
            makeCoffeeButton.setBackground(getDrawable(R.drawable.roundedbutton));
            makeCoffeeButton.setTextColor(getResources().getColor(R.color.colorAccent));
            makeCoffeeButton.setText(R.string.make_coffee);
        }
    }

}
